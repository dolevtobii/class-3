#include <iostream>
#include <string>
#include "Vector.h"


using std::cout;
using std::cin;
using std::endl;
using std::string;

#define MIN_SIZE 2
#define EMPTY_VECTOR -9999

/*
	funcction is a constructor 
	input: the size of the Vector
	output: none
*/
Vector::Vector(int n)
{
	if(n < MIN_SIZE)
	{
		n = MIN_SIZE;
	}

	this->_resizeFactor = n;
	this->_elements = new int[n];
	this->_capacity = n;
	this->_size = 0;
}


/*
	funcction will delete the memory of the class
	input: none
	output: none
*/
Vector::~Vector()
{
	delete[] _elements;
}


/*
	funcction will return the size of the Vector
	input: none
	output: the size of the Vector
*/
int Vector::size() const
{
	return this->_size;
}


/*
	funcction will return the capacity of the Vector
	input: none
	output: the capacity of the Vector
*/
int Vector::capacity() const
{
	return this->_capacity;
}


/*
	funcction will return the resizeFactor of the Vector
	input: none
	output: the resizeFactor of the Vector
*/
int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}


/*
	funcction will check if the Vector is empty
	input: none
	output: true or false if the Vector is empty
*/
bool Vector::empty() const
{
	bool result = false;

	if (this->_size == 0)
	{
		result = true;
	}

	return result;
}


/*
	function will add an element to the end of the array
	input: val- the element
	output: none
*/
void Vector::push_back(const int& val)
{
	int* tempArr = NULL;

	int i = 0;
	
	if (this->_size < this->_capacity)
	{
		this->_elements[this->_size] = val;
		this->_size++;
	}
	else if (this->_size >= this->_capacity)
	{		
		tempArr = new int[this->_capacity + this->_resizeFactor];
		for (i = 0; i < this->_size; i++)
		{
			tempArr[i] = this->_elements[i];
		}

		tempArr[i] = val;

		delete[] this->_elements;
		this->_elements = NULL;
		this->_size++;

		this->_elements = tempArr;
		this->_capacity += this->_resizeFactor;
		
	}
	
}


/*
	function will delete the last element in the array
	input: none
	output: the deleted element
*/
int Vector::pop_back()
{
	int result = 0;
	
	if(this->_size == 0)
	{
		cout << "error: pop from empty vector" << endl;
		result = EMPTY_VECTOR;
	}
	else
	{
		result = this->_elements[this->_size - 1];
		this->_size--;
	}

	return result;
}


/*
	function will reserve capacity for the array
	input: n- the mew capacity
	output: none
*/
void Vector::reserve(int n)
{
	int* tempArr = NULL;
	int i = 0;

	if (n > this->_capacity)
	{
		do
		{
			this->_capacity += this->_resizeFactor;
		} while (this->_capacity < n);

		tempArr = new int[this->_capacity];

		for (i = 0; i < this->_size; i++)
		{
			tempArr[i] = this->_elements[i];
		}

		delete[] this->_elements;
		this->_elements = NULL;

		this->_elements = tempArr;
	}

}


/*
	function will resize the array by n given
	input: n- the new size of the array
	outout: none
*/
void Vector::resize(int n)
{
	if (n <= this->_capacity)
	{
		this->_size = n;
	}
	else if (n > this->_capacity)
	{
		this->reserve(n);
	}
}


/*
	function will assign the value to all the empty elements in vectors
	input: val- the value
	output: none
*/
void Vector::assign(int val)
{
	int i = 0;

	for(i = this->_size; i < this->_capacity; i++)
	{
		this->_elements[i] = val;
	}
	
}


/*
	function will resize the vector 
	input: n- the new size
		   val- the value to insert the empty spaces
	output: none
*/
void Vector::resize(int n, const int& val)
{
	int i = 0;

	if (n <= this->_capacity)
	{
		for (i = this->_size; i < n; i++)
		{
			this->_elements[i] = val;
		}

		this->_size = n;
	}
	else if (n > this->_capacity)
	{
		this->reserve(n);
		for (i = this->_size; i < this->_capacity; i++)
		{
			this->_elements[i] = val;
		}
	}

}


/*
	function will copy the vector 1 to vector 2
	input: other- the vector 1
	output: none
*/
Vector::Vector(const Vector& other)
{
	int i = 0;

	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;

	this->_elements = new int[other._capacity];

	for(i = 0; i < other._capacity; i++)
	{
		this->_elements[i] = other._elements[i];
	}
}


/*
	function will put the values of vector 1 in vectir 2
	input: other- vector 1
	output: vector 2
*/
Vector& Vector::operator=(const Vector& other)
{
	int i = 0;

	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;

	delete[] this->_elements;

	this->_elements = new int[other._capacity];

	for (i = 0; i < other._capacity; i++)
	{
		this->_elements[i] = other._elements[i];
	}

	return *this;
}


/*
	function will return the value of the vector in n index
	input: n- the index
	output: the value of n index
*/
int& Vector::operator[](int n) const
{
	int result = 0;

	if (this->_size > n)
	{
		result = this->_elements[n];
	}
	else
	{
		result = this->_elements[0];
		cout << "\nindex out of range" << endl;
	}

	return result;
}


//----------------------------------------------------------------------------------------
//my functions

/*
	funtion will print the given array
	input: arr- the array
	output: none
*/
void Vector::print_array()
{
	int i = 0;
	
	for (i = 0; i < this->_size; i++)
	{
		cout << "element: " << this->_elements[i] << endl;
	}
	cout << "\n\n" << endl;
}